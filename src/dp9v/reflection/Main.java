package dp9v.reflection;

import dp9v.reflection.classloader.CustomClassloader;
import dp9v.reflection.serializer.ObjectRecord;

import javax.xml.parsers.ParserConfigurationException;
import java.net.MalformedURLException;
import java.util.HashMap;

/**
 * Created by dpolkovnikov on 10.02.17.
 */
public class Main {
	public static void main(String[] args) throws MalformedURLException, IllegalAccessException, InstantiationException {
		CustomClassloader cl = new CustomClassloader();
		Class cls = null;
		try {
			cls = cl.loadClass("Animal");
		}
		catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		Object o = cls.newInstance();



		//People p = new People("Ya", 23, 9999999.);
		//HashMap<Integer, Float> array2 = new HashMap<>();
		//array2.put(1,1.1F);
		//array2.put(2,2.2F);
		//array2.put(4,5.1F);
		ObjectRecord obj = new ObjectRecord(o);
		try {
			Serializer.serilize(obj, "txt/text.xml");
		}
		catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
	}
}
