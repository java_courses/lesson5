package dp9v.reflection;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class People implements Serializable{
	static private People2 ppp = new People2();
	private ArrayList<Integer> array = new ArrayList<>();
	private HashMap<Integer, Float> array2 = new HashMap<>();
	//private int[][] array2 = {{1,2,3}, {1,2,3}};
	private String name;
	private Integer age;
	private Double salary;

	public People(String name, Integer age, Double salary) {
		array.add(1);
		array.add(2);
		array2.put(1,1.1F);
		array2.put(2,2.2F);
		array2.put(4,5.1F);
		this.name = name;
		this.age = age;
		this.salary = salary;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

	protected void paySalry(){
		System.out.println("I have salary " + this.getSalary());
	}
}
