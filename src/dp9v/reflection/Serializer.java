package dp9v.reflection;

import dp9v.reflection.serializer.ObjectRecord;
import dp9v.reflection.serializer.PrimitiveFieldRecord;
import org.w3c.dom.Document;
import org.w3c.dom.Element;


import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

/**
 * Created by dpolkovnikov on 10.02.17.
 */
public class Serializer {
	private static final String OBJECT_TAG_NAME = "object";
	private static final String FIELD_TAG_NAME = "field";

	public static void serilize(ObjectRecord obj, String path) throws ParserConfigurationException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		Document doc = factory.newDocumentBuilder().newDocument();
		doc.appendChild(createObjectTag(obj, doc));
		saveToFile(path, doc);

	}

	private static Element createObjectTag(ObjectRecord obj, Document doc){
		Element objectTag = doc.createElement(OBJECT_TAG_NAME);
		objectTag.setAttribute("name", obj.getName());
		for (PrimitiveFieldRecord simpleField:
				obj.getPrimitiveFields()){
			objectTag.appendChild(createFieldTag(simpleField, doc));
		}
		for (ObjectRecord objectField:
				obj.getObjectFields()){
			objectTag.appendChild(createObjectTag(objectField, doc));
		}
		return objectTag;
	}

	private static Element createFieldTag(PrimitiveFieldRecord obj, Document doc){
		Element field = doc.createElement(FIELD_TAG_NAME);
		field.setAttribute("name", obj.getName());
		field.setAttribute("type", obj.getType());
		field.setAttribute("value", obj.getValue());
		return field;
	}

	private static void saveToFile(String path, Document doc){
		DOMSource source = new DOMSource(doc);
		try {
			StreamResult result = new StreamResult(new FileOutputStream(path));
			TransformerFactory transFactory = TransformerFactory.newInstance();
			Transformer transformer = transFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
			transformer.transform(source, result);
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch (TransformerConfigurationException e) {
			e.printStackTrace();
		}
		catch (TransformerException e) {
			e.printStackTrace();
		}
	}
}
