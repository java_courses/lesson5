package dp9v.reflection.classloader;

/**
 * Created by dpolkovnikov on 15.02.17.
 */
public class Animal_tmp {
	private String name;
	private String type;
	private int age;

	public Animal_tmp(String name, String type, int age) {
		this.name = name;
		this.type = type;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
}
