package dp9v.reflection.classloader;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.JarURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Hashtable;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Created by dpolkovnikov on 15.02.17.
 */
public class CustomClassloader extends ClassLoader {
	private String                   classPath = "jar:https://gitlab.com/java_courses/lesson5/raw/master/src/Animal.jar!/";
	private Hashtable<String, Class> classes   = new Hashtable<>();


	public CustomClassloader() {
		super(CustomClassloader.class.getClassLoader());
	}

	@Override
	protected Class<?> findClass(String name) throws ClassNotFoundException {
		return loadClass(name);
	}

	@Override
	public Class<?> loadClass(String name) throws ClassNotFoundException {
		byte[] classBytes;
		Class  result = null;

		result = classes.get(name);
		if(result != null)
			return result;

		try {
			return findSystemClass(name);
		}
		catch (Exception ex) {}

		try {
			URL                   urlJar        = new URL(classPath);
			JarURLConnection      jarConnection = (JarURLConnection) urlJar.openConnection();
			JarFile               jar           = jarConnection.getJarFile();
			JarEntry              entry         = jar.getJarEntry(name + ".class");
			InputStream           is            = jar.getInputStream(entry);
			ByteArrayOutputStream bs            = new ByteArrayOutputStream();
			int                   nextValue     = 0;
			while((nextValue = is.read()) != -1) {
				bs.write(nextValue);
			}

			classBytes = bs.toByteArray();
		}
		catch (IOException ex) {
			ex.printStackTrace();
			return null;
		}
		result = defineClass(name, classBytes, 0, classBytes.length);
		classes.put(name, result);
		return result;
	}


}
