package dp9v.reflection.serializer;

/**
 * Created by dpolkovnikov on 10.02.17.
 */
public class FieldRecord {
	private String type;
	private String name;

	public FieldRecord(String type, String name) {
		this.type = type;
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
