package dp9v.reflection.serializer;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;

/**
 * Created by dpolkovnikov on 10.02.17.
 */
public class ObjectRecord extends FieldRecord{
	private ArrayList<PrimitiveFieldRecord> primitiveFields;
	private ArrayList<ObjectRecord>         objectFields;
	private boolean isArray = false;


	public ObjectRecord(Object obj){
		super(obj.getClass().getName(), obj.getClass().getTypeName());
		this.primitiveFields = new ArrayList<>(16);
		this.objectFields = new ArrayList<>(16);
		Field[] fields = obj.getClass().getDeclaredFields();
		for(Field f:
		    fields) {
			if(!Modifier.isTransient(f.getModifiers()))
				addField(obj,f);
		}
	}

	public ObjectRecord(String name, String type){
		super(name, type);
		isArray = true;
		this.primitiveFields = new ArrayList<>(16);
		this.objectFields = new ArrayList<>(16);
	}

	public ArrayList<PrimitiveFieldRecord> getPrimitiveFields() {
		return primitiveFields;
	}

	public ArrayList<ObjectRecord> getObjectFields() {
		return objectFields;
	}

	public void addPrimitiveField(PrimitiveFieldRecord f){
		primitiveFields.add(f);
	}

	public void addObjectField(ObjectRecord f){
		objectFields.add(f);
	}

	public void addPrimitiveField(Object obj, Field f){
		PrimitiveFieldRecord newField = new PrimitiveFieldRecord(obj, f);
		primitiveFields.add(newField);
	}

	public void addObjectField(Object obj, Field f){
		try {
			f.setAccessible(true);
			ObjectRecord newObject = new ObjectRecord(f.get(obj));
			this.objectFields.add(newObject);
		}catch (IllegalAccessException ex){
			ex.printStackTrace();
		}
	}

	public void addArray(Object obj, Field f){
		ObjectRecord record = new ObjectRecord(f.getName(), f.getType().getComponentType().toString());
		f.setAccessible(true);
		int length = 0;
		try {
			length = Array.getLength(f.get(obj));
			for (int i = 0; i < length; i ++) {
				Object arrayElement = Array.get(f.get(obj), i);
				System.out.println(arrayElement);
			}
		}
		catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		System.out.println(length);
	}

	public void addField(Object obj, Field f){
		if (isPrimitive(f))
			addPrimitiveField(obj, f);
		else if (f.getType().isArray())
			addArray(obj, f);
		else
			addObjectField(obj, f);
	}



	public static boolean isPrimitiveWrapper(Field f){
		Class s = f.getType();
		if(s == Boolean.class ||
		        s == Byte.class ||
				s == Short.class ||
				s == Integer.class ||
				s == Long.class ||
				s == Double.class ||
				s == Float.class ||
				s == String.class){
			return true;
		}
		return false;
	}

	public static boolean isPrimitive(Field f){
		Class s = f.getType();
		return s.isPrimitive() || isPrimitiveWrapper(f);
	}
}
