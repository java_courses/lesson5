package dp9v.reflection.serializer;

import java.lang.reflect.Field;

/**
 * Created by dpolkovnikov on 10.02.17.
 */
public class PrimitiveFieldRecord extends FieldRecord {
	private String value;

	public PrimitiveFieldRecord(String type, String name, String value) {
		super(type, name);
		this.value = value;
	}

	public PrimitiveFieldRecord(Object obj, Field f){
		super(f.getType().toString(), f.getName());
		f.setAccessible(true);
		String value = null;
		try {
			value = f.get(obj).toString();
		}
		catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		this.setValue(value);
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
