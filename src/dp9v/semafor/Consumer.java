package dp9v.semafor;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by dpolkovnikov on 10.02.17.
 */
public class Consumer {
	MySemaphore kubSem=new MySemaphore(),
			kvadratSem=new MySemaphore(),
			prostSem=new MySemaphore();

	private AtomicInteger sum = new AtomicInteger();

	public void message(int cub, int quad, int simple){
		System.out.print(cub+ " - " + quad + " - " + simple + " = ");
		if (cub!=0)
			addNumber(kubSem, cub);
		else if(quad!=0)
			addNumber(kubSem, quad);
		else
			addNumber(kubSem, simple);

	}

	private void addNumber(MySemaphore sem, int num){
		sem.take();
		int new_sum = sum.addAndGet(num);
		System.out.println(new_sum);
		sem.release();
	}

}
