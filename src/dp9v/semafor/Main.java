package dp9v.semafor;

import dp9v.semafor.calculators.Kubator;
import dp9v.semafor.calculators.Kvadrator;
import dp9v.semafor.calculators.Prostator;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by dpolkovnikov on 10.02.17.
 */
public class Main {
	public static void main(String[] args) {
		Consumer cons = new Consumer();

		ArrayList<Integer> arr1 = new ArrayList<>(1280);
		ArrayList<Integer> arr2 = new ArrayList<>(1280);
		Random random = new Random();
		for(int i=0; i<1200; i++){
			arr1.add(random.nextInt(10)+1);
			arr2.add(random.nextInt(10)+1);
		}

		Thread th1 = new Thread(new Kubator(arr1, cons));
		Thread th2 = new Thread(new Kvadrator(arr1, cons));
		Thread th3 = new Thread(new Prostator(arr1, cons));

		th1.run();
		th2.run();
		th3.run();
	}
}
