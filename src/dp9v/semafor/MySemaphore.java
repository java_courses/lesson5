package dp9v.semafor;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by dpolkovnikov on 10.02.17.
 */
public class MySemaphore {
	private AtomicBoolean takeFlag = new AtomicBoolean();

	public void take() {
		while(!takeFlag.compareAndSet(false, true)){
		}
	}

	public void release(){
		takeFlag.set(false);
	}
}