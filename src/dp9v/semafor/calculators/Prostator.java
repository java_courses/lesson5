package dp9v.semafor.calculators;

import dp9v.semafor.Consumer;
import dp9v.semafor.MySemaphore;

import java.util.ArrayList;

/**
 * Created by dpolkovnikov on 10.02.17.
 */
public class Prostator implements Runnable {

	private ArrayList<Integer> numbers;
	private Consumer        consumer;

	public Prostator(ArrayList<Integer> numbers, Consumer consumer) {
		this.numbers = numbers;
		this.consumer = consumer;
	}

	@Override
	public void run() {
		for(Integer num:
				numbers) {
			consumer.message(0, 0, num);
		}
	}
}
